package com.example.m7_m8_project2_xaviguillamon

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(val id: Int, var name: String, var url: String): Parcelable

