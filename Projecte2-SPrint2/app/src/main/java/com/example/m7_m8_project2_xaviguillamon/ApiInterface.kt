package com.example.m7_m8_project2_xaviguillamon

import android.provider.ContactsContract.Data
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    companion object {
        val BASE_URL = "https://api.genshin.dev/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }

    @GET("weapons")
    fun getData(): Call<Weapons>

    @GET("weapons/{name}")
    fun getSingleWeaponData(@Path("name") name: String): Call<Data>

}