package com.example.m7_m8_project2_xaviguillamon

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Repository {
    private val apiInterface = ApiInterface.create()
    fun getWeaponList(): List<String> {
        var weaponsList: List<String> = listOf()
        val call = apiInterface.getData()
        call.enqueue(object: Callback<Weapons> {
            override fun onFailure(call: Call<Weapons>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }
            override fun onResponse(call: Call<Weapons?>, response: Response<Weapons?>) {
                if (response != null && response.isSuccessful) {
                    weaponsList = response.body()!!
                }
            }
        })
        return weaponsList
    }


}