package com.example.m7_m8_project2_xaviguillamon

import com.google.gson.annotations.SerializedName

data class SingleWeaponData(
    val ascensionMaterial: String,
    val baseAttack: Int,
    @SerializedName("location")
    val location: String,
    val name: String,
    val passiveDesc: String,
    val passiveName: String,
    val rarity: Int,
    val subStat: String,
    val type: String
)