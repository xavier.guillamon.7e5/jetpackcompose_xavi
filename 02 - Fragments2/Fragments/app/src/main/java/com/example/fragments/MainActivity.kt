package com.example.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val f1button = findViewById<Button>(R.id.button)
        f1button.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView2, Fragment1())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }

        val f2button = findViewById<Button>(R.id.button2)
        f2button.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView2, Fragment2())
                setReorderingAllowed(true)
                addToBackStack("name") // name can be null
                commit()
            }
        }


    }
}
