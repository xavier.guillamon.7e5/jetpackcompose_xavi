package com.example.m7_m8_project2_xaviguillamon

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    /*private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null*/

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*layoutManager = LinearLayoutManager(this)

        RecyclerViewID.layoutManager = layoutManager

        adapter = RecyclerAdapter()
        RecyclerViewID*/
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, RecyclerViewFragment())
            setReorderingAllowed(true)
            addToBackStack("name") // name can be null
            commit()
        }
    }
}