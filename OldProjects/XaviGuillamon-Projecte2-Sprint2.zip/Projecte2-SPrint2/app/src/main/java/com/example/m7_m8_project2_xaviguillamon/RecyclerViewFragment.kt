package com.example.m7_m8_project2_xaviguillamon

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.m7_m8_project2_xaviguillamon.databinding.FragmentRecyclerViewBinding

class RecyclerViewFragment : Fragment(), OnClickListener {
    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding
    private var idArr = arrayOf("1", "2", "3", "4", "5", "6", "7")
    private var namesArr = arrayOf("Xiao", "Raiden", "Itto", "Hu Tao", "Bennet", "Eula", "Yelan")
    private var urlArr = arrayOf(
        "https://wallpapers-clan.com/wp-content/uploads/2022/09/genshin-impact-xiao-pfp-1.jpg",
        "https://pbs.twimg.com/media/FMR75oPVcAQHXus.jpg",
        "https://64.media.tumblr.com/070db1f60b8377f7b7e892ef99b1996c/6aa4a528f4671fd9-32/s500x750/2729d1ec8dc01b00450e30e509546e3de52d285d.jpg",
        "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/71ca6060-71b6-40d1-b66f-4fa942c80004/dfdbi1i-8738c943-37e1-4f42-af9e-8a32fdfeb523.jpg/v1/fill/w_893,h_894,q_70,strp/hu_tao_insta_discord_yt_pfp__by_bigsur1234_dfdbi1i-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9OTA0IiwicGF0aCI6IlwvZlwvNzFjYTYwNjAtNzFiNi00MGQxLWI2NmYtNGZhOTQyYzgwMDA0XC9kZmRiaTFpLTg3MzhjOTQzLTM3ZTEtNGY0Mi1hZjllLThhMzJmZGZlYjUyMy5qcGciLCJ3aWR0aCI6Ijw9OTAzIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.UeftuqYpgFZl9fhixwe2Z1Jz0b1iHWxDliGTEFc_C9M",
        "https://images4.alphacoders.com/114/1140989.jpg",
        "https://i.pinimg.com/736x/77/34/a9/7734a94ba5cbc84e6fa0bc072ef90117.jpg",
        "https://avatarfiles.alphacoders.com/318/thumb-318841.png")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewBinding.inflate(inflater,container, false)
        return binding.root
    }

    private fun getUsers(): MutableList<User>{
        val users = mutableListOf<User>()
        /*
        for (i in 1..7) {
            users.add(User(idArr[i], namesArr[i], urlArr[i]))
        }*/
        users.add(User(1, "Xiao", "https://wallpapers-clan.com/wp-content/uploads/2022/09/genshin-impact-xiao-pfp-1.jpg"))
        users.add(User(2, "Raiden", "https://pbs.twimg.com/media/FMR75oPVcAQHXus.jpg"))
        users.add(User(3, "Itto", "https://64.media.tumblr.com/070db1f60b8377f7b7e892ef99b1996c/6aa4a528f4671fd9-32/s500x750/2729d1ec8dc01b00450e30e509546e3de52d285d.jpg"))
        users.add(User(4, "Hu Tao", "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/71ca6060-71b6-40d1-b66f-4fa942c80004/dfdbi1i-8738c943-37e1-4f42-af9e-8a32fdfeb523.jpg/v1/fill/w_893,h_894,q_70,strp/hu_tao_insta_discord_yt_pfp__by_bigsur1234_dfdbi1i-pre.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9OTA0IiwicGF0aCI6IlwvZlwvNzFjYTYwNjAtNzFiNi00MGQxLWI2NmYtNGZhOTQyYzgwMDA0XC9kZmRiaTFpLTg3MzhjOTQzLTM3ZTEtNGY0Mi1hZjllLThhMzJmZGZlYjUyMy5qcGciLCJ3aWR0aCI6Ijw9OTAzIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.UeftuqYpgFZl9fhixwe2Z1Jz0b1iHWxDliGTEFc_C9M"))
        users.add(User(5, "Bennet", "https://images4.alphacoders.com/114/1140989.jpg"))
        users.add(User(6, "Eula", "https://i.pinimg.com/736x/77/34/a9/7734a94ba5cbc84e6fa0bc072ef90117.jpg"))
        users.add(User(7, "Yelan", "https://avatarfiles.alphacoders.com/318/thumb-318841.png"))

        return users
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userAdapter = UserAdapter(getUsers(), this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }
    }
    override fun onClick(user: User) {
        parentFragmentManager.setFragmentResult(
            "User", bundleOf("User" to user)
        )
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }
    }
}