/*package com.example.m7_m8_project2_xaviguillamon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.R.drawable.*
import com.example.m7_m8_project2_xaviguillamon.R.drawable.*

class RecyclerAdapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var idArr = arrayOf("1", "2", "3", "4", "5", "6", "7")
    private var namesArr = arrayOf("Xiao", "Raiden", "Itto", "Hu Tao", "Bennet", "Eula", "Yelan")
    private var imagesArr = arrayOf(xiao, raiden, itto, hutao, bennet, eula, yelan)

    override fun getItemCount(): Int {
        return namesArr.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_recycler_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemName.text = namesArr[position]
        holder.itemImage.setImageResource(imagesArr[position])
        holder.itemId.text = idArr[position]
    }
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView
        var itemName: TextView
        var itemId: TextView

        init {
            itemImage = itemView.findViewById(R.id.imageView)
            itemName = itemView.findViewById(R.id.nameView)
            itemId = itemView.findViewById(R.id.idView)
        }
    }

}*/