package com.example.m7_m8_project2_xaviguillamon

interface OnClickListener {
    fun onClick(user: User)
}
