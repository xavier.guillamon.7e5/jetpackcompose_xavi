package com.example.composeactivity

import android.os.Bundle
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.ui.Modifier
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.VerticalAlignmentLine
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composeactivity.ui.theme.ComposeActivityTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeActivityTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colors.background
                ) {
                    MyApp()
                }
            }
        }
    }
}


@Composable
fun MyApp() {
    Scaffold(
        content = {
            //Greeting(name = "Xiao")
            OlidProject()
        }
    )
}

@Composable
fun Greeting(name: String) {
    Box(modifier = Modifier.fillMaxSize()) {
        Image(
            painter = painterResource(R.drawable.background),
            contentDescription = "Background",
            modifier = Modifier.fillMaxWidth(),
            contentScale = ContentScale.Crop
        )
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Image(
                    painter = painterResource(id = R.drawable.smolxiao),
                    contentDescription = "Xiao Profile Picture",
                    modifier = Modifier
                        .padding(20.dp)
                        .size(200.dp)
                        .clip(shape = CutCornerShape(corner = CornerSize(10.dp)))
                )
                Text(text = "Hello $name!", color = Color.White)
            }
            Image(
                painter = painterResource(id = R.drawable.bigxiao),
                contentDescription = "Xiao Background",
                modifier = Modifier
                    .padding(20.dp)
                    .clip(shape = RoundedCornerShape(corner = CornerSize(30.dp)))
            )
        }
    }

}

@Composable
fun OlidProject() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(start = 16.dp, top = 16.dp, bottom = 24.dp, end = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.smolxiao),
            contentDescription = "Xiao Profile Picture",
            modifier = Modifier
                .size(250.dp)
                .clip(shape = CircleShape)
        )
        Text(
            text = "Genshin Impact",
            modifier = Modifier
                .padding(top = 16.dp)
        )
        Text(
            text = "Genshin Impact[b] is an action role-playing game developed and published by miHoYo. It was released for Android, iOS, PlayStation 4, " +
                    "and Windows in 2020, on PlayStation 5 in 2021, and is set for release on Nintendo Switch. The game features an anime-style open-world " +
                    "environment and an action-based battle system using elemental magic and character-switching.",
            modifier = Modifier
                .padding(top = 16.dp)
        )
        Row(
            verticalAlignment = Alignment.Bottom,
            horizontalArrangement = Arrangement.End,
            modifier = Modifier.fillMaxSize()
        ) {
            Button(onClick = {}, shape = CutCornerShape(10)) {
                Text(text = "Xiao")
            }
            Spacer(modifier = Modifier.padding(8.dp))
            Button(onClick = {}, shape = CutCornerShape(10)) {
                Text(text = "Itto")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ComposeActivityTheme {
        Greeting("Android")
    }
}